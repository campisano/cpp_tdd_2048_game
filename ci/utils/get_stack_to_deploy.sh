#!/usr/bin/env bash

set -o errexit -o nounset -o pipefail

PROJECT_NAME="$(./ci/custom/get_project_name.sh)"
DEPLOY_NAME="$(echo "${PROJECT_NAME}" | sed 's/_/-/g')"
DEPLOY_VERSION="$(./ci/utils/get_project_version.sh)"
DEPLOY_IMAGE="${DOCKER_REGISTRY_URL}:${PROJECT_NAME}-${DEPLOY_VERSION}"

export DEPLOY_NAME DEPLOY_IMAGE DEPLOY_VERSION



for F in kubernetes/*; do echo '---'; cat "$F"; done | envsubst '$DEPLOY_NAME $DEPLOY_IMAGE $DEPLOY_VERSION'
