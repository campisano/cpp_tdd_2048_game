binary_name			:= 2048
type				:= exec

include_paths			:= lib/doctest/install/include
library_paths			:=

source_paths			:= src/2048/domain src/2048/application src/2048/application/ports/in src/2048/application/ports/out src/2048/application/usecases src/2048/adapters src/common
libs_to_link			:=

main_source_paths		:= src/main

test_source_paths		:= src/test src/test/domain src/test/domain/doubles src/test/application
test_add_libs_to_link		:=

subcomponent_paths		:=

PREFIX				:= $(CURDIR)/install

include lib/generic_makefile/modular_makefile.mk
